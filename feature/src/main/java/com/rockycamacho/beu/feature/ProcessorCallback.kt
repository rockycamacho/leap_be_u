package com.rockycamacho.beu.feature

import com.google.firebase.ml.vision.face.FirebaseVisionFace

interface ProcessorCallback {

    fun onFacesDetected(faces: MutableList<FirebaseVisionFace>)

    fun onException(e: Exception)

}