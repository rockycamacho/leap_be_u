package com.rockycamacho.beu.feature

import android.Manifest
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.google.firebase.ml.vision.face.FirebaseVisionFace
import com.tbruyelle.rxpermissions2.RxPermissions
import io.fotoapparat.Fotoapparat
import io.fotoapparat.characteristic.LensPosition
import io.fotoapparat.log.logcat
import io.fotoapparat.log.loggers
import io.fotoapparat.parameter.ScaleType
import kotlinx.android.synthetic.main.activity_main.camera
import kotlinx.android.synthetic.main.activity_main.status


class MainActivity : AppCompatActivity(), ProcessorCallback {

    private var fotoapparat: Fotoapparat? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val rxPermissions = RxPermissions(this)
        val frameProcessor = BeUFrameProcessor(this, this)
        rxPermissions
            .request(Manifest.permission.CAMERA)
            .subscribe({ granted ->
                           if (granted) {

                               fotoapparat = Fotoapparat.with(this)
                                   .into(camera)
                                   .frameProcessor(frameProcessor)
                                   .lensPosition { LensPosition.Back }
                                   .previewScaleType(ScaleType.CenterCrop)
                                   .logger(loggers(logcat()))
                                   .cameraErrorCallback { error ->
                                       Log.e("Camera",
                                             error.message,
                                             error)
                                   }
                                   .build()
                               fotoapparat!!.start()
                           }
                       }, { error ->
                           Log.e("Main", error.message, error)
                       })
    }

    override fun onResume() {
        super.onResume()
        fotoapparat?.start()
    }

    override fun onPause() {
        fotoapparat?.stop()
        super.onPause()
    }

    override fun onFacesDetected(faces: MutableList<FirebaseVisionFace>) {
        var totalSmilingProbability = 0f
        for (face in faces) {
            totalSmilingProbability += face.smilingProbability
            Log.d("Happiness", "Happiness: ${face.smilingProbability * 100}%" )
        }
        val averageSmilingProbability = totalSmilingProbability / faces.size
        status.text = "Average Happiness Probability %: $averageSmilingProbability"
    }

    override fun onException(e: Exception) {
        Log.e("onException", e.message, e)
    }
}

